﻿using System;
using System.IO;
using Newtonsoft.Json;
using PaymentApplication.Model;

namespace PaymentApplication.Data
{
    public static class PaymentSaver
    {
        public static void SaveToJson(CreditCard creditCard, decimal amount, string path)
        {
            string filename = $"payment{DateTime.Now.Year}{DateTime.Now.Month}{DateTime.Now.Day}-{DateTime.Now.Hour}{DateTime.Now.Minute}{DateTime.Now.Second}.{DateTime.Now.Millisecond}.json";
            using (StreamWriter file = File.CreateText($@"{path}\{filename}"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, new
                {
                    creditCard, amount, processDateTime = DateTime.Now
                });
            }
        }
    }
}
