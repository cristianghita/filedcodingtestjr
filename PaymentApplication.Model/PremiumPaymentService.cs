﻿using PaymentApplication.Model.Interface;

namespace PaymentApplication.Model
{
    public class PremiumPaymentService : IPaymentService
    {
        public void ProcessPayment(CreditCard creditCard, decimal amount)
        {
            //throw new System.Exception("test");
            System.Console.Out.WriteLine($"{StaticStrings.PremiumPayment} has been processed successfully.");
        }
    }
}
