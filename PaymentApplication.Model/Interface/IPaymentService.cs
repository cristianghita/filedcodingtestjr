﻿namespace PaymentApplication.Model.Interface
{
    public interface IPaymentService
    {
        void ProcessPayment(CreditCard creditCard, decimal amount);
    }
}
