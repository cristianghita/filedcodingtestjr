﻿namespace PaymentApplication.Model
{
    /// <summary>
    /// This class allows setting hardcoded strings and enables the flexibility of
    /// changing them over the whole app in case that's needed.
    /// </summary>
    public static class StaticStrings
    {
        public static readonly string CheapPayment = "Cheap Payment";
        public static readonly string ExpensivePayment = "Expensive Payment";
        public static readonly string PremiumPayment = "Premium Payment";
    }
}
