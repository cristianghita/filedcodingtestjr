﻿using PaymentApplication.Model.Interface;

namespace PaymentApplication.Model
{
    public class ExpensivePaymentService : IPaymentService
    {
        public void ProcessPayment(CreditCard creditCard, decimal amount)
        {
            //throw new System.Exception("test");
            System.Console.Out.WriteLine($"{StaticStrings.ExpensivePayment} has been processed successfully.");
        }
    }
}
