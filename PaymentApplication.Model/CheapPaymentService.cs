﻿using PaymentApplication.Model.Interface;

namespace PaymentApplication.Model
{
    public class CheapPaymentService : IPaymentService
    {
        public void ProcessPayment(CreditCard creditCard, decimal amount)
        {
            //throw new System.Exception("test");
            System.Console.Out.WriteLine($"{StaticStrings.CheapPayment} has been processed successfully.");
        }
    }
}
