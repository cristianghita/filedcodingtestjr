﻿namespace PaymentApplication.Model.Exception
{
    public class InvalidAmountException : System.Exception
    {
        public InvalidAmountException(string message) : base(message)
        {
            
        }
    }
}
