﻿namespace PaymentApplication.Model.Exception
{
    public class InvalidCardException : System.Exception
    {
        public InvalidCardException(string message) : base(message)
        {
            
        }
    }
}
