﻿using System;

namespace PaymentApplication.Model
{
    public class CreditCard
    {
        public string CreditCardNumber { get; }
        public string CardHolder { get; }
        public DateTime ExpirationDate { get; }
        public int? SecurityCode { get; }

        public CreditCard(string creditCardNumber, string cardHolder, DateTime expirationDate, int? securityCode = null)
        {
            CreditCardNumber = creditCardNumber;
            CardHolder = cardHolder;
            ExpirationDate = expirationDate;
            SecurityCode = securityCode;
        }
    }
}
