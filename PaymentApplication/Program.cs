﻿using System;
using PaymentApplication.Data;
using PaymentApplication.Model;
using PaymentApplication.Model.Exception;

namespace PaymentApplication
{
    public class Program
    {
        private static int _maxPremiumPaymentAttempts = 3;
        private static readonly string AppExeDirectory = AppDomain.CurrentDomain.BaseDirectory; // the path where the payment file(s) will be saved

        static void Main()
        {
            // The following 2 scenarios were requested in the brief 
            ProcessPayment(string.Empty, "John Doe", DateTime.MaxValue, 20);
            Console.Out.WriteLine();
            ProcessPayment("1234567890123456", "John Doe", DateTime.MinValue, 5);
            Console.Out.WriteLine();
            
            // The following will process one payment of each type
            // Each successful payment will generated a JSON file in the directory where the executable is (bin)
            // You can test the catch statements by uncommenting the "throw new Exception.." inside each PaymentService class
            ProcessPayment("1234567890123456", "John Doe", DateTime.MaxValue, 5);
            Console.Out.WriteLine();
            
            ProcessPayment("1234567890123456", "John Doe", DateTime.MaxValue, 25);
            Console.Out.WriteLine();
            
            ProcessPayment("1234567890123456", "John Doe", DateTime.MaxValue, 501);
            Console.Out.WriteLine();
            
            Console.ReadKey(); // used to hold the program in order to see the stuff printed to console
        }

        /// <summary>
        /// This function will validate a card and will process a payment on that card.
        /// </summary>
        /// <param name="creditCardNumber">16 digits credit card number.</param>
        /// <param name="cardHolder">Card holder name in string format.</param>
        /// <param name="expirationDate">Expiration date in datetime format higher than current datetime.</param>
        /// <param name="amount">Decimal transaction amount.</param>
        /// <param name="securityCode">Card security code (usually 3 digits).</param>
        static void ProcessPayment(string creditCardNumber, string cardHolder, DateTime expirationDate, decimal amount,
            int? securityCode = null)
        {
            var creditCard = new CreditCard(creditCardNumber, cardHolder, expirationDate, securityCode); // Create CreditCard object with passed details
            try
            {
                // Validate card and amount
                ValidateCreditCard(creditCard);
                ValidateAmount(amount);

                if (amount < 21)
                    ProcessCheapPayment(creditCard, amount);
                else if (amount >= 21 && amount <= 500)
                    ProcessExpensivePayment(creditCard, amount);
                else
                    ProcessPremiumPayment(creditCard, amount);
            }
            catch (InvalidAmountException ex)
            {
                Console.Out.WriteLine($"There was a problem with payment amount.{Environment.NewLine}{ex.Message}");
            }
            catch (InvalidCardException ex)
            {
                Console.Out.WriteLine($"There was a problem with the credit card.{Environment.NewLine}{ex.Message}");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine($"There was an unidentified problem.{Environment.NewLine}{ex.Message}");
            }

        }
        
        /// <summary>
        /// Validates the mandatory fields of a credit card.
        /// </summary>
        /// <param name="creditCard">CreditCard to be validated.</param>
        static void ValidateCreditCard(CreditCard creditCard)
        {
            if (creditCard == null) throw new ArgumentNullException(nameof(creditCard));

            if (string.IsNullOrEmpty(creditCard.CreditCardNumber) || creditCard.CreditCardNumber.Length != 16)
                throw new InvalidCardException("CreditCardNumber should contain 16 digits.");

            if (string.IsNullOrEmpty(creditCard.CardHolder))
                throw new InvalidCardException("CardHolder is a mandatory field.");

            if (creditCard.ExpirationDate <= DateTime.Now )
                throw new InvalidCardException("ExpirationDate should be a future date.");
        }

        /// <summary>
        /// Validates the passed amount value.
        /// </summary>
        /// <param name="amount">Transaction amount.</param>
        static void ValidateAmount(decimal amount)
        {
            if (amount < 0) throw new InvalidAmountException("The amount should be positive.");
        }

        /// <summary>
        /// Processes payment via CheapPaymentService and saves the payment.
        /// </summary>
        /// <param name="creditCard">CreditCard class containing the credit card details.</param>
        /// <param name="amount">Transaction amount.</param>
        static void ProcessCheapPayment(CreditCard creditCard, decimal amount)
        {
            var cheapPaymentService = new CheapPaymentService();
            try
            {
                cheapPaymentService.ProcessPayment(creditCard, amount);
                PaymentSaver.SaveToJson(creditCard, amount, AppExeDirectory); 
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine($"The {StaticStrings.CheapPayment} is not available.{Environment.NewLine}{ex}");
            }
        }

        /// <summary>
        /// Processes payment via ExpensivePaymentService, retries via CheapPaymentService if that
        /// does not work and saves the payment.
        /// </summary>
        /// <param name="creditCard">CreditCard class containing the credit card details.</param>
        /// <param name="amount">Transaction amount.</param>
        static void ProcessExpensivePayment(CreditCard creditCard, decimal amount)
        {
            var expensivePaymentService = new ExpensivePaymentService();
            try
            {
                expensivePaymentService.ProcessPayment(creditCard, amount);
                PaymentSaver.SaveToJson(creditCard, amount, AppExeDirectory);
            }
            catch
            {
                Console.Out.WriteLine($"The {StaticStrings.ExpensivePayment} is not available. Will proceed to retry the payment via {StaticStrings.CheapPayment}");
                ProcessCheapPayment(creditCard, amount);
            }
        }

        /// <summary>
        /// Processes payment via PremiumPaymentService and retries _maxPremiumPaymentAttempts times if it fails.
        /// </summary>
        /// <param name="creditCard">CreditCard class containing the credit card details.</param>
        /// <param name="amount">Transaction amount.</param>
        static void ProcessPremiumPayment(CreditCard creditCard, decimal amount)
        {
            var attempts = 0;
            while (attempts < _maxPremiumPaymentAttempts + 1) // +1 because first try is not a retry (max 3 retries = a total of 4 tries)
            {
                var premiumPaymentService = new PremiumPaymentService();
                try
                {
                    premiumPaymentService.ProcessPayment(creditCard, amount);
                    PaymentSaver.SaveToJson(creditCard, amount, AppExeDirectory);
                    return;
                }
                catch
                {
                    Console.Out.WriteLine($"The {StaticStrings.PremiumPayment} is not available.");
                    if (attempts < _maxPremiumPaymentAttempts)
                        Console.Out.WriteLine($"The payment will retry to be processed. Attempt no: {attempts + 1}");
                    attempts++;
                }
            }
            
            if(attempts == _maxPremiumPaymentAttempts + 1) Console.Out.WriteLine($"The {StaticStrings.PremiumPayment} has NOT been processed successfully.");
        }
    }
}
